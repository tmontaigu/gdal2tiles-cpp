//
// Created by Thomas on 13/12/2018.
//

#ifndef GDAL2TILES_CPP_GLOBALMERCATOR_H
#define GDAL2TILES_CPP_GLOBALMERCATOR_H

#define _USE_MATH_DEFINES
#include <math.h>

#include <tuple>

class GlobalMercator {
public:
	/* Returns tile for given mercator coordinates */
	std::tuple<int, int> meters_to_tile(double xm, double ym, int zoom) const;
	/* Converts EPSG:3857 to pyramid pixel coordinates in given zoom level*/
	std::tuple<double, double> meters_to_pixel(double xm, double ym, int zoom) const;
	/* "Returns a tile covering region in given pixel coordinates" */
	std::tuple<int, int> pixel_to_tile(double px, double py) const;

	std::tuple<double, double> pixels_to_meters(int px, int py, int zoom) const;
	std::tuple<double, double, double, double> tile_bounds(int tx, int ty, int zoom) const;

	std::tuple<double, double> meters_to_lat_lon(double xm, double ym) const;



	int zoom_for_pixel_size(double pixel_size) const;

	double resolution(int zoom) const;

	double tile_size() const { return m_tile_size; }

private:
    int m_tile_size = 256;
    double m_initial_resolution = 2 * M_PI * 6378137 / m_tile_size;
    double m_original_shift = 2 * M_PI * 6378137 / 2.0;
};


#endif //GDAL2TILES_CPP_GLOBALMERCATOR_H
