//
// Created by Thomas on 13/12/2018.
//

#include "GlobalMercator.h"

#include <tuple>
#include <algorithm>

std::tuple<int, int> GlobalMercator::meters_to_tile(double xm, double ym, int zoom) const
{
	auto[px, py] = meters_to_pixel(xm, ym, zoom);
	return pixel_to_tile(px, py);
}

std::tuple<double, double> GlobalMercator::meters_to_pixel(double xm, double ym, int zoom) const
{
	double res = resolution(zoom);
	return { (xm + m_original_shift) / res, (ym + m_original_shift) / res };
}

std::tuple<int, int> GlobalMercator::pixel_to_tile(double px, double py) const
{
	auto tx = static_cast<int>(ceil(px / double(m_tile_size)) - 1);
	auto ty = static_cast<int>(ceil(py / double(m_tile_size)) - 1);
	return { tx, ty };
}

std::tuple<double, double> GlobalMercator::pixels_to_meters(int px, int py, int zoom) const
{
	double res = resolution(zoom);
	return { px * res - m_original_shift, py * res - m_original_shift };
}

std::tuple<double, double, double, double> GlobalMercator::tile_bounds(int tx, int ty, int zoom) const
{
	const auto[minx, miny] = pixels_to_meters(tx * m_tile_size, ty * m_tile_size, zoom);
	const auto[maxx, maxy] = pixels_to_meters((tx + 1) * m_tile_size, (ty + 1) * m_tile_size, zoom);
	return { minx, miny, maxx, maxy };
}

std::tuple<double, double> GlobalMercator::meters_to_lat_lon(double xm, double ym) const
{
	double lon = (xm / m_original_shift) * 180.0;
	double lat = (ym / m_original_shift) * 180.0;

	lat = 180.0 / M_PI * (2 * atan(exp(lat * M_PI / 180.0)) - M_PI / 2.0);

	return { lat, lon };
}

int GlobalMercator::zoom_for_pixel_size(double pixel_size) const
{
	for (int i = 0; i < 32; i++)
	{
		if (pixel_size > resolution(i))
		{
			return std::max(0, i - 1);
		}
	}
	return 32;
}

double GlobalMercator::resolution(int zoom) const
{
	return m_initial_resolution / pow(2, zoom);
}
