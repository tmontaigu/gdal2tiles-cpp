#include <iostream>
#include <sstream>
#include <array>
#include <filesystem>
#include <numeric>
#include <fstream>

#include<time.h>

#include "GlobalMercator.h"

//#include <gdal.h>
#include <gdal_priv.h>
#include <gdalwarper.h>
#include "cpl_conv.h"
#include <ogr_spatialref.h>

#include <json/json.h>
#include <thread>

const char* OUT_DRIVER_NAME = "PNG";
const size_t MAX_ZOOM_LEVEL = 32;
const size_t TILE_SIZE = 256;
const size_t MAX_CACHE_SIZE = 1000000;

struct Options {
	std::string out_folder = ".";
	std::string resampling = "near";
};

int query_size_for_resampling(const std::string& resampling)
{
	if (resampling == "near")
	{
		return TILE_SIZE;
	}
	else if (resampling == "bilinear")
	{
		return TILE_SIZE * 2;
	}
	else
	{
		return TILE_SIZE * 4;
	}
}

struct TileDetail {
	int tx = 0;
	int ty = 0;
	int tz = 0;

	int rx = 0;
	int ry = 0;
	int rxsize = 0;
	int rysize = 0;

	int wx = 0;
	int wy = 0;
	int wxsize = 0;
	int wysize = 0;

	int query_size = 0;
};

struct TileMinMax {
	int xmin = 0;
	int xmax = 0;
	int ymin = 0;
	int ymax = 0;

	int tile_count() const { return (1 + abs(xmax - xmin)) * (1 + (ymax - ymin)); }
};

void generate_metadata(const Options& opts, std::array<double, 4>& datasetLatLon)
{
	std::cout << "Generating metadata\n";
	Json::Value root;

	root["name"] = "LOL";
	root["format"] = "png";

	root["minzoom"] = 22;
	root["maxzoom"] = 22;
	std::string bounds_str = std::to_string(datasetLatLon[1]) + "," + std::to_string(datasetLatLon[0]) + "," + std::to_string(datasetLatLon[3]) + "," + std::to_string(datasetLatLon[2]);
	root["bounds"] = bounds_str.c_str();

	//std::cout << root << std::endl;

	std::ofstream metadata_file;
	metadata_file.open(opts.out_folder + "/metadata.json");
	metadata_file << root << std::endl;
	metadata_file.close();
}

 TileDetail geo_query(double ulx, double uly, double lrx, double lry, GDALDataset* ds, int query_size = 0)
{
	std::array<double, 6> geo_transform;
	ds->GetGeoTransform(geo_transform.data());

	int rx = static_cast<int>((ulx - geo_transform[0]) / geo_transform[1] + 0.001);
	int ry = static_cast<int>((uly - geo_transform[3]) / geo_transform[5] + 0.001);
	int rxsize = static_cast<int>((lrx - ulx) / geo_transform[1] + 0.5);
	int rysize = static_cast<int>((lry - uly) / geo_transform[5] + 0.5);

	//printf("first rx ry %d %d\n", rx, ry);

	int wxsize = rxsize;
	int wysize = rysize;

	if (query_size)
	{
		wxsize = wysize = query_size;
	}

	// Coordinates should not go out of the bounds of the raster

	int wx = 0;
	if (rx < 0)
	{
		int rxshift = abs(rx);
		wx = wxsize * (double(rxshift) / rxsize);
		wxsize = wxsize - wx;
		rxsize = rxsize - (rxsize * (double(rxshift) / rxsize));
		rx = 0;
	}
	if (rx + rxsize > ds->GetRasterXSize())
	{
		//std::cout << rx << " " << rxsize << " "<< ds->GetRasterXSize() << '\n';
		wxsize = wxsize * (double(ds->GetRasterXSize() - rx) / rxsize);
		rxsize = ds->GetRasterXSize() - rx;
		//printf("new rxsize: %d\n", rxsize);
	}

	int wy = 0;
	if (ry < 0)
	{
		int ryshift = abs(ry);
		wy = wysize * (double(ryshift) / rysize);
		wysize = wysize - wy;
		rysize = rysize - (rysize * (double(ryshift) / rysize));
		ry = 0;
	}
	if (ry + rysize > ds->GetRasterYSize())
	{
		wysize = wysize * (double(ds->GetRasterYSize() - ry) / rysize);
		rysize = ds->GetRasterYSize() - ry;
	}
	
	TileDetail details;
	details.rx = rx;
	details.ry = ry;
	details.rxsize = rxsize;
	details.rysize = rysize;
	details.wx = wx;
	details.wy = wy;
	details.wxsize = wxsize;
	details.wysize = wysize;

	return details;
}

std::vector<TileDetail> generate_base_tiles_info(const std::array<TileMinMax, MAX_ZOOM_LEVEL>& tminmax, int max_zoom, const GlobalMercator& mercator, GDALDataset *ds)
{
	const TileMinMax& max_tile = tminmax.at(max_zoom);

	int tile_count = max_tile.tile_count();
	int current_tile_index = 0;

	std::vector<TileDetail> base_tiles_details;
	base_tiles_details.reserve(tile_count);
	for (int ty = max_tile.ymax; ty >= max_tile.ymin; ty--)
	{
		for (int tx = max_tile.xmin; tx < max_tile.xmax + 1; tx++)
		{
			current_tile_index++;

			//std::cout << current_tile_index << " / " << tile_count << '\n';
			//std::cout << "\tTile name: " << copyOfStr << '\n';

			const auto[bxmin, bymin, bxmax, bymax] = mercator.tile_bounds(tx, ty, max_zoom);
			//std::cout << "mercator tile bounds " << tx << " " << ty << " " << bxmin << " " << bymin << " " << bxmax << " " << bymax << '\n';
			TileDetail details = geo_query(bxmin, bymax, bxmax, bymin, ds, mercator.tile_size());
			details.tx = tx;
			details.ty = ty;
			details.tz = max_zoom;
			base_tiles_details.push_back(details);
		}
	}
	//std::cout << "did " << current_tile_index << " tiles\n";
	return base_tiles_details;
}

int nb_raster_band(GDALDataset *ds)
{
	int raster_count = ds->GetRasterCount();
	if (raster_count != 0)
	{
		bool hasAlpha = ds->GetRasterBand(1)->GetMaskFlags() & GMF_ALPHA;
		if (hasAlpha || raster_count == 2 || raster_count == 4)
		{
			return raster_count - 1;
		}
	}
	return raster_count;
}

bool all_raster_bands_have_same_type(GDALDataset *ds)
{
	//in GAL>=2.3 there will be a new GetBands iterator
	GDALDataType first_type = ds->GetRasterBand(1)->GetRasterDataType();
	for (int i(1); i < ds->GetRasterCount(); ++i)
	{
		GDALRasterBand *band = ds->GetRasterBand(i);
		if (band->GetRasterDataType() != first_type)
			return false;
	}
	return true;
}

void create_base_tile(const TileDetail& details, GDALDataset *ds, const Options& opts, void *buf = nullptr)
{
	int nb_band = nb_raster_band(ds);
	int add_alpha = 1;
	GDALDriver* mem_driver =  GetGDALDriverManager()->GetDriverByName("MEM");
	GDALDriver *out_driver = GetGDALDriverManager()->GetDriverByName(OUT_DRIVER_NAME);


	GDALRasterBand *in_band = ds->GetRasterBand(1);
	GDALDataType in_band_type = in_band->GetRasterDataType();
	//std::cout << "out mem nb raster" << out_memdataset->GetRasterCount() << '\n';

	bool was_allocated = false;
	if (buf == nullptr)
	{
		buf = CPLMalloc(GDALGetDataTypeSizeBytes(in_band_type) * details.rxsize * details.rysize * nb_band);
		bool was_allocated = true;
	}
	

	GDALRasterBand *in_alpha_band = in_band->GetMaskBand();
	in_alpha_band->AdviseRead(details.rx, details.ry, details.rxsize, details.rysize, details.wxsize, details.wysize, GDALDataType::GDT_Byte, NULL);
	auto err = in_alpha_band->RasterIO(GF_Read, details.rx, details.ry, details.rxsize, details.rysize, buf,  details.wxsize, details.wysize, GDALDataType::GDT_Byte, 0, 0, NULL);
	if (err != CE_None)
	{
		std::cout << "Error reading alpha_band\n";
		return;
	}

	auto ualpha = static_cast<uint8_t*>(buf);
	if (false && std::all_of(ualpha, ualpha + (details.rxsize * details.rysize), [](const uint8_t val) { return val == 0; }))
	{
		return;
	}

	std::string filename(opts.out_folder + "/" + std::to_string(details.tz) + "/" + std::to_string(details.tx) + "/" + std::to_string(details.ty) + ".png");
	GDALDataset *out_memdataset = mem_driver->Create(filename.c_str(), 256, 256, nb_band + add_alpha, GDALDataType::GDT_Byte, NULL);
	if (add_alpha)
	{
		err = out_memdataset->CreateMaskBand(GMF_ALPHA | GMF_PER_DATASET);
		if (err != CE_None)
		{
			std::cout << "Failed to create alpha ba reading alpha_band\n";
			return;
		}
		GDALRasterBand *out_band = out_memdataset->GetRasterBand(1);
		GDALRasterBand* out_alpha_band = out_band->GetMaskBand();
		std::array<int, 1> band_list{ nb_band + 1 };
		err = out_memdataset->RasterIO(GF_Write, 0, 0, details.wxsize, details.wysize, buf,  details.wxsize, details.wysize, GDALDataType::GDT_Byte, 1 /* nb band*/,band_list.data(), 0,0,0, NULL);
		if (err != CE_None)
		{
			std::cout << "Failed to write to the alpha band\n";
			return;
		}
	}

	GDALRasterIOExtraArg extra_args;
	INIT_RASTERIO_EXTRA_ARG(extra_args);
	err = ds->RasterIO(GF_Read, details.rx, details.ry, details.rxsize, details.rysize, buf,  details.wxsize, details.wysize, in_band_type, nb_band, NULL, 0,0,0, NULL);
	if (err != CE_None)
	{
		std::cout << "Error reading rasterband\n";
		return;
	}

	err = out_memdataset->RasterIO(GF_Write, 0, 0, details.wxsize, details.wysize, buf,  details.wxsize, details.wysize, in_band_type, nb_band, NULL, 0,0,0, NULL);
	if (err != CE_None)
	{
		std::cout << "Error writing rasterband\n";
		return;
	}
	out_driver->CreateCopy(filename.c_str(), out_memdataset, TRUE /* strict */, NULL, NULL, NULL);

	GDALClose(out_memdataset);
	if (was_allocated)
		CPLFree(buf);
}

void create_base_tiles(const std::vector<TileDetail> details, GDALDataset *ds, const Options opts, void *buf = nullptr)
{
	for (const auto& detail : details)
	{
		create_base_tile(detail, ds, opts, buf);
	}
}


void generate_overview_tiles(const Options& opts, const std::vector<TileMinMax>& tminmax, int max_zoom, int min_zoom)
{
	int tile_size = 256;
	GDALDriver* mem_driver =  GetGDALDriverManager()->GetDriverByName("MEM");
	GDALDriver *out_driver = GetGDALDriverManager()->GetDriverByName(OUT_DRIVER_NAME);

	int tile_count = std::accumulate(
		std::begin(tminmax) + min_zoom,
		std::end(tminmax) - max_zoom,
		0,
		[](const int count, const TileMinMax& t) { return count + t.tile_count(); }
	);

	size_t current_tile = 0;
		
	for (int tz = max_zoom - 1; tz > min_zoom - 1; --tz)
	{
		const TileMinMax& current_zoom = tminmax[tz];
		const TileMinMax& more_zoomed = tminmax[tz + 1];
		for (int ty = current_zoom.ymax; ty > current_zoom.ymin; --ty)
		{
			for (int tx = current_zoom.xmin; tx <= current_zoom.xmax; ++tx)
			{
				current_tile++;
				std::string filename(opts.out_folder + "/" + std::to_string(tz) + "/" + std::to_string(tx) + "/" + std::to_string(ty) + ".png");


				for (size_t y = pow(2, ty); y < pow(2, ty) + 2; ++y)
				{
					for (size_t x = pow(2, tx); x < pow(2, tx); ++x)
					{
						if (x < more_zoomed.xmin || x > more_zoomed.xmax || y < more_zoomed.ymin || y > more_zoomed.ymax) {
							continue;
						}
						std::string base_filename(opts.out_folder + "/" + std::to_string(tz + 1) + "/" + std::to_string(tx) + "/" + std::to_string(ty) + ".png");
						auto ds_querytile = static_cast<GDALDataset*>(GDALOpen(base_filename.c_str(), GA_ReadOnly));
						if (!ds_querytile) {
							continue;
						}

						int tileposy = 0;
						if ((ty == 0 && y == 1) || (ty != 0 && (y % 2 * ty) != 0))
							tileposy = 0;
						else
							tileposy = tile_size;

						int tileposx = 0;
						if (tx)
							tileposx = x % (2 * tx) * tile_size;
						else if (tx == 0 && x == 1)
							tileposx = tile_size;
					}
				}
					
			}
		}
	}

}

void create_directory_tree(const std::array<TileMinMax, 32>& tminmax, int zoom, const Options& opt)
{
	const TileMinMax& current = tminmax.at(zoom);
	std::filesystem::path zoom_dir_path(opt.out_folder + "/" + std::to_string(zoom));

	std::filesystem::create_directory(zoom_dir_path);

	for (int tx = current.xmin; tx < current.xmax + 1; tx++)
	{
		// A copy is needed not to append recursively to the zomm_dir_path
		auto tmp_p = zoom_dir_path;
		std::filesystem::path x_dir_path = tmp_p.append(std::to_string(tx));
		std::filesystem::create_directories(x_dir_path);
	}
}

GDALDataset *copy_in_mem(GDALDataset *warped_dataset)
{
	//GDALDriver *png_dr = GetGDALDriverManager()->GetDriverByName(OUT_DRIVER_NAME);
	//png_dr->CreateCopy("E:\\mdr.png", warped_dataset, FALSE, NULL, NULL, NULL);

	GDALDriver* dr = GetGDALDriverManager()->GetDriverByName("MEM");
	auto *lol = dr->CreateCopy("E:\\lol.png", warped_dataset, FALSE, NULL, NULL, NULL);
	if (lol)
		return lol;


	int X = warped_dataset->GetRasterXSize();
	int Y = warped_dataset->GetRasterYSize();
	int B = warped_dataset->GetRasterCount();
	std::cout << X << "x" << Y << "w" << B << '\n';
	GDALDataType T = warped_dataset->GetRasterBand(1)->GetRasterDataType();

	GDALDataset *ds = dr->Create("E:\\lol.png", X, Y, B + 1, GDALDataType::GDT_Byte, NULL);
	if (!ds)
	{
		std::cout << "fail ds\n";
		return nullptr;
	}

	void *cpy_buf = CPLMalloc(X * B * Y * GDALGetDataTypeSizeBytes(T));
	if (!cpy_buf)
		throw std::bad_alloc();

	auto err = ds->CreateMaskBand(GMF_ALPHA | GMF_PER_DATASET);
	if (err != CE_None)
	{
		std::cout << "fail add mask\n";
		return nullptr;
	}

	err = warped_dataset->RasterIO(GF_Read, 0, 0, X, Y, cpy_buf,  X, Y, T, B, NULL, 0,0,0, NULL);
	if (err != CE_None)
	{
		std::cout << "fuck\n";
		return nullptr;
	}

	err = ds->RasterIO(GF_Write, 0, 0, X, Y, cpy_buf,  X, Y, GDT_Byte, B, NULL, 0,0,0, NULL);
	if (err != CE_None)
	{
		std::cout << "fuck\n";
		return nullptr;
	}

	err = warped_dataset->GetRasterBand(1)->GetMaskBand()->RasterIO(GF_Read, 0, 0, X, Y, cpy_buf, X, Y, T, 0, 0, NULL);
	if (err != CE_None)
	{
		std::cout << "fuck\n";
		return nullptr;
	}
	std::array<int, 1> band_list = { B + 1};
	err = ds->RasterIO(GF_Write, 0, 0, X, Y, cpy_buf,  X, Y, GDALDataType::GDT_Byte, 1 /* nb band*/,band_list.data(), 0,0,0, NULL);
	if (err != CE_None)
	{
		std::cout << "fuck\n";
		return nullptr;
	}
	CPLFree(cpy_buf);
	return ds;
}

int main(int argc, char *argv[]) {
    if (argc < 3) {
        std::cout << "Wrong arg\n";
        return -1;
    } else {
        std::cout << "Filename: " << argv[1] << "\n";
		std::cout << "OutFolder: " << argv[2] << '\n';
    }
	Options opts;
	opts.out_folder = argv[2];

    GDALAllRegister();

    if (!GDALGetDriverByName(OUT_DRIVER_NAME))
    {
        std::cout << "Driver for '" << OUT_DRIVER_NAME << "' was not found.";
        std::cout << " Is it available in this build ?\n";
        return -1;
    }

	if (!GDALGetDriverByName("MEM"))
    {
        std::cout << "Driver for '" << "MEM" << "' was not found.";
        std::cout << " Is it available in this build ?\n";
        return -1;
    }

    std::unique_ptr<GDALDataset> dataset((GDALDataset*)GDALOpen(argv[1], GA_ReadOnly));
    if (dataset == nullptr)
    {
        std::cout << "Failed to open the dataset";
        return -1;
    }

	OGRSpatialReference in_srs(dataset->GetProjectionRef());
	if (in_srs.Validate() != OGRERR_NONE)
	{
        std::cout << "Input file has invalid srs\n";
        return -1;
	}

	if (dataset->GetRasterCount() == 0)
	{
		std::cout << "Input dataset must have at least one band\n";
		return -1;
	}

	if (!all_raster_bands_have_same_type(dataset.get()))
	{
		std::cout << "All bands must have same datatype\n";
		return -1;
	}

	OGRSpatialReference out_srs;
	out_srs.importFromEPSG(3857);
	char *out_wkt = nullptr;
	char *in_wkt = nullptr;
	in_srs.exportToWkt(&in_wkt);
	out_srs.exportToWkt(&out_wkt);

	std::unique_ptr<GDALDataset> warped_dataset;
	if (!EQUAL(out_wkt, in_wkt)) 
	{
		//FIXME where is the warped dataset (in mem or on disk?) what does dataset become?
		warped_dataset.reset(static_cast<GDALDataset*>(GDALAutoCreateWarpedVRT(dataset.get(), in_wkt, out_wkt, GRA_NearestNeighbour, 0.0, NULL)));
		if (!warped_dataset) 
		{
			std::cout << "Failed to warp the input dataset\n";
			return -1;
		}
	}
	else
	{
		warped_dataset = std::move(dataset);
	}

	std::array<double, 6> geo_transform;
	warped_dataset->GetGeoTransform(geo_transform.data());

	if (geo_transform[2] != 0.0 || geo_transform[4] != 0.0)
	{
		std::cout << "Raster contains skew, it is not supported\n";
		return -1;
	}

	//check that geo_transform pixel size is square
	double ominx = geo_transform[0];
	double omaxx = geo_transform[0] + warped_dataset->GetRasterXSize() * geo_transform[1];
	double omaxy = geo_transform[3];
	double ominy = geo_transform[3] - warped_dataset->GetRasterYSize() * geo_transform[1];

	std::cout << "omaxx - ominx: " << omaxx - ominx << '\n';
	std::cout << "omaxy - ominy: " << omaxy - ominy << '\n';

	GlobalMercator mercator;

	// Generate table with min max tile coordinates for all zoomlevels
	std::array<TileMinMax, MAX_ZOOM_LEVEL > tminmax;
	for (size_t i(0); i < tminmax.size(); ++i) {
		TileMinMax& current = tminmax[i];
		std::tie(current.xmin, current.ymin) = mercator.meters_to_tile(ominx, ominy,i);
		std::tie(current.xmax, current.ymax) = mercator.meters_to_tile(omaxx, omaxy,i);
		//crop tiles extending world limits(+-180, +-90)
		current.xmin = std::max(0, current.xmin);
		current.ymin = std::max(0, current.ymin);
		current.xmax = std::min(static_cast<int>(pow(2, i) - 1), current.xmax);
		current.ymax = std::min(static_cast<int>(pow(2, i) - 1), current.ymax);
	}

	// Find the min zoom level (one tile covers whole area)
	int longest = std::max(warped_dataset->GetRasterXSize(), warped_dataset->GetRasterYSize());
	double pixel_size_min = geo_transform[1] * longest / double(mercator.tile_size());
	int tminz = mercator.zoom_for_pixel_size(pixel_size_min);

	//Find the max zoom level
	int tmaxz = mercator.zoom_for_pixel_size(geo_transform[1]);

	std::cout << "Min Zoom level: " << tminz << " Max Zoom level: " << tmaxz << '\n';

	// METADATA
	auto [south, west] = mercator.meters_to_lat_lon(ominx, ominy);
	auto [north, east] = mercator.meters_to_lat_lon(omaxx, omaxy);
	south = std::max(-85.05112878, south);
	west = std::max(-180.0, west);
	north = std::min(85.05112878, north);
	east = std::min(180.0, east);
	std::array<double, 4> datasetLatLon{ south, west, north, east };
	generate_metadata(opts, datasetLatLon);

	create_directory_tree(tminmax, tmaxz, opts);


	// Generate base tile
	std::vector<TileDetail> base_tiles_details = generate_base_tiles_info(tminmax, tmaxz, mercator, warped_dataset.get());

	std::cout << "Num base tiles (tiles at max zoom): " << base_tiles_details.size() << '\n';

	size_t max_buffer_size = 0;
	GDALDataType band_data_type = warped_dataset->GetRasterBand(1)->GetRasterDataType();
	auto band_datasize = static_cast<size_t>(GDALGetDataTypeSizeBytes(band_data_type));
	std::cout << "Band size in bytes: " << band_datasize << '\n';
	std::cout << "num bands: " << nb_raster_band(warped_dataset.get()) << '\n';


	for (const TileDetail& tile : base_tiles_details)
	{
		max_buffer_size = std::max(max_buffer_size, band_datasize * tile.rxsize * tile.rysize * nb_raster_band(warped_dataset.get()));
	}
	std::cout << "Max buffer_size: " << max_buffer_size << "\n";

	//std::cout << "start copy\n";
	//auto *ds = copy_in_mem(dataset.get());
	//if (!ds)
	//{
	//	std::cout << "fail copy in mem\n";
	//	return 0;
	//}
	//std::cout << "copy ok\n";
	//auto *wds = static_cast<GDALDataset*>(GDALAutoCreateWarpedVRT(ds, in_wkt, out_wkt, GRA_NearestNeighbour, 0.0, NULL));
	//std::cout << "warped\n";
	//void * buf = CPLMalloc(max_buffer_size);
	//for (const TileDetail& tile : base_tiles_details)
	//{
	//	create_base_tile(tile, warped_dataset.get(), opts, buf);
	//}


	size_t num_threads = 2;
	size_t num_tiles = base_tiles_details.size();
	std::vector<std::vector<TileDetail>> details_for_threads(num_threads);
	std::div_t d = std::div(int(num_tiles), int(num_threads));
	std::cout << details_for_threads.size() << "\n";
	std::cout << num_tiles << " -> " << d.quot << ", " << d.rem << "\n";

	for (const auto& v: details_for_threads)
	{
		std::cout << "size: " << v.size() << "\n";
	}

	for (size_t i = 0; i < num_threads; ++i)
	{
		std::vector<TileDetail>& tiles_for_thread = details_for_threads.at(i);
		details_for_threads.reserve(num_tiles);
		for (size_t j = i * d.quot; j < (i + 1) * d.quot; ++j)
		{
			//std::cout << "th " << i << " -> " << j << '\n';
			const auto lol = base_tiles_details.at(j);
			//tiles_for_thread.push_back(lol);
			details_for_threads[i].push_back(lol);
		}

		/*if (i == num_threads  -1)
		{
			for (size_t j = num_tiles - d.rem; j < num_tiles; ++j)
			{
				tiles_for_thread.push_back(base_tiles_details.at(j));
			}
		}*/
	}
	std::cout << "lol\n";
	std::vector<std::thread> th;
	auto mdr = [&warped_dataset, &opts](std::vector<TileDetail> dd) {create_base_tiles(dd, warped_dataset.get(), opts); };
	for (const auto& v: details_for_threads)
	{
		const Options &oo(opts);
		std::cout << "size: " << v.size() << "\n";

		std::thread t(mdr, v);

		th.push_back(std::move(t));
	}

	for (auto& thread : th)
	{
		thread.join();
	}
	return 0;
}