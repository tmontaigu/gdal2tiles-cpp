cmake_minimum_required(VERSION 3.10)
project(gdal2tiles_cpp)

set(CMAKE_CXX_STANDARD 17)

add_executable(gdal2tiles_cpp src/main.cpp src/GlobalMercator.cpp)

set(GDAL_INCLUDE_DIR C:/Users/t.montaigu/AppData/Local/Continuum/miniconda3/envs/condaenv/Library/include)
set(GDAL_LIBRARY "C:/Users/t.montaigu/AppData/Local/Continuum/miniconda3/envs/condaenv/Library/lib/gdal_i.lib")

find_package(jsoncpp CONFIG REQUIRED)


#include_directories("${JSONCPP_INCLUDE_DIRS}")
include_directories( ${GDAL_INCLUDE_DIR} )
target_link_libraries( gdal2tiles_cpp ${GDAL_LIBRARY} jsoncpp_lib)
#target_link_libraries(gdal2tiles_cpp PRIVATE jsoncpp_lib)

